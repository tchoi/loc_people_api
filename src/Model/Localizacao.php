<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 12/05/2018
 * Time: 16:56
 */

namespace LocPeopleApi\Domain\Model;
class Localizacao extends AbstractModel{
  protected $connection = 'mysql_main';
  protected $table = 'localizacao';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = ['usuario_id','dh_registro','dh_fix','latitude','longitude','direcao','precisao','velocidade','altitude','endereco'];
}