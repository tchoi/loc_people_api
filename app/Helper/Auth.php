<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:58
 */

namespace LocPeopleApi\App\Helper;


use app\UnauthorizedException;
use LocPeopleApi\Domain\Model\Usuario;

class Auth
{

  public static function getUserByToken($token)
  {

    $user = (new \LocPeopleApi\Domain\Repository\UsuarioRepository())->getUserByToken($token);
    if (!$user) {
      /**
       * The throwable class must implement UnauthorizedExceptionInterface
       */
      throw new UnauthorizedException('Invalid Token');
    }
    return $user->toArray();
  }
}