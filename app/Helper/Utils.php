<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:19
 */

namespace LocPeopleApi\App\Helper;


class Utils {
  public static function postUrlRapid($url,$params=array()){
    $content = http_build_query($params);
    $context = stream_context_create(array(
      'http' => array(
        'method'  => 'POST',
        'content' => $content,
      )
    ));
    $contents = file_get_contents($url, null, $context);
    return $contents;
  }
  /**
   * Determine if supplied string is a valid GUID
   *
   * @param string $guid String to validate
   * @return boolean
   */
  public static function isValidGuid($guid){
    return !empty($guid) && preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/', $guid);
  }
}