<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 12/05/2018
 * Time: 17:13
 */

namespace LocPeopleApi\App\Controller;

use LocPeopleApi\App\Exception\HttpException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
class LocalizacaoController extends BaseController{
  public function create(Request $request, Response $response, $args)
  {
    $ret = [
        'statusCode' => 401,
        'success' => false,
        'msg' => "Erro ao tentar salvar a localizacao"
    ];
    $data = null;
    $data = $request->getParsedBody();
    $user = $this->authLogon($data);
    if (!$user['success']) {
      $ret['msg'] = $ret['msg'] . "\n" . implode("\n", $user['errors']);
      return json_encode($ret);
    }
    $user = $user['user'];

    $localizacoes = json_decode($data['localizacoes'],true);

    foreach($localizacoes as $loc){
      $loc['usuario_id'] = $user->id;
      if(!$this->repository("localizacao")->verifyIfLocationSaved($loc)){
        $save = $this->repository("localizacao")->createLocalizacao($loc);
      }else{
        $save = true;
      }

      if(!$save){
        break;
      }
    }
    if($save!==false){
      $ret['statusCode'] = 201;
      $ret['success'] = true;
      $ret['msg'] = "Localizacao salva com sucesso";
      $ret['user'] = $save;
    }
    return json_encode($ret);
  }
  public function filter(Request $request, Response $response, $args)
  {
    $ret = [
        'statusCode' => 401,
        'success' => false,
        'msg' => "Erro ao tentar buscar as localizações"
    ];
    $data = null;
    $data = $request->getParsedBody();
    $user = $this->authLogon($data);
    if (!$user['success']) {
      $ret['msg'] = $ret['msg'] . "\n" . implode("\n", $user['errors']);
      return json_encode($ret);
    }
    $user = $user['user'];
    $data['usuario_id'] = $user->id;
    $locs = $this->repository("localizacao")->getLocalizacoesByFiltro($data);

    $ret['statusCode'] = 201;
    $ret['success'] = true;
    $ret['msg'] = "Localizações";
    $ret['locs'] = $locs;
    return json_encode($ret);
  }
}