<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 12/05/2018
 * Time: 16:57
 */

namespace LocPeopleApi\Domain\Repository;


use LocPeopleApi\Domain\Model\Localizacao;

class LocalizacaoRepository
{
  public function createLocalizacao($data){
    $loc = new Localizacao();
    $loc->fill($data);
    if($loc->save()){
      return $loc;
    }
    return false;
  }
  public function verifyIfLocationSaved($data){
    $loc = Localizacao::where("usuario_id",$data['usuario_id'])
        ->where("dh_registro",$data['dh_registro'])
        ->first();
    if(isset($loc->id)){
      return true;
    }
    return false;
  }
  public function getLocalizacoesByFiltro($data){
    $locs = Localizacao::join("usuario","usuario.id","=","localizacao.usuario_id")
        ->select('localizacao.*','usuario.nome')
        ->where('localizacao.dh_registro','>=',$data['data_inicio'])
        ->where('localizacao.dh_registro','<=',$data['data_fim'])
        ->where('localizacao.usuario_id','>=',$data['usuario_id'])
        ->orderBy('localizacao.dh_registro','ASC')
        ->get()->toArray();
    return $locs;
  }
}