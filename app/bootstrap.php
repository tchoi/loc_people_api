<?php
include __DIR__ . '/../vendor/autoload.php';

define('SETTINGS_FILE', __DIR__ . '/settings.json');

if (!function_exists('pr')) {
  function pr() {
    $args = func_get_args();
    foreach ($args as $arg) {
      if (php_sapi_name() == "cli") {
        echo print_r($arg, true)."\n";
      }else{
        echo implode('', [
          '<div style="border: solid 1px #000; background-color: #eee;margin: 10px 0px;padding: 10px 15px;"><pre>',
          print_r($arg, true),
          '</pre></div>'
        ]);
      }
    }
  }
}

final class Bootstrap
{
  private static $app = null;
  private static $settings = null;

  public static function init(){
    if (empty(self::$app)) {
      $app = new \Slim\App([
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
      ]);

      self::loadSettings();
      $container = $app->getContainer();
      $container['database'] = self::$settings->database;


      include __DIR__ . '/repositories.php';
      include __DIR__ . '/services.php';
      include __DIR__ . '/routes.php';

      self::loadConnections($container);
      self::$app = $app;
    }

    return self::$app;
  }

  public static function initTerminal(){

    if (empty(self::$app)) {
      $argv = $GLOBALS['argv'];
      array_shift($GLOBALS['argv']);
      array_shift($argv);
      $pathInfo = "/".implode('/', $argv);
      $env = \Slim\Http\Environment::mock(['REQUEST_URI' => $pathInfo]);

      $authenticator = function($request, TokenAuthentication $tokenAuth){
        # Search for token on header, parameter, cookie or attribute
        $token = $tokenAuth->findToken($request);

        # Your method to make token validation
        $user = LocPeopleApi\App\Helper\Auth::getUserByToken($token);

      };

      $app = new \Slim\App([
        'environment'=>$env
      ]);



      self::loadSettings();

      $container = $app->getContainer();
      $container['database'] = self::$settings->database;

      include __DIR__ . '/repositories.php';
      include __DIR__ . '/services.php';
      include __DIR__ . '/routeTerminal.php';

      self::loadConnections($container);

      self::$app = $app;
    }

    return self::$app;

  }

  private static function loadSettings()
  {
    if (!is_readable(SETTINGS_FILE)) {
      throw new \Exception("Impossível ler arquivo settings.json");
    }

    self::$settings = json_decode(file_get_contents(SETTINGS_FILE));
  }

  private static function loadConnections($container)
  {
    $capsule = new \LocPeopleApi\App\Ext\Capsule();
    foreach ((array) $container['database'] as $connectionName => $connectionConfig) {
      $capsule->addConnection((array) $connectionConfig,$connectionName);
      $capsule->setNameConnection($connectionName);
    }

    /*$capsule->getDatabaseManager()->extend('oracle', function ($config) {
      $connector = new Yajra\Oci8\Connectors\OracleConnector();
      $connection = $connector->connect($config);
      $db = new QInstall\App\Ext\Oci8Connection($connection, $config['database'], $config['prefix']);
      $sessionVars = [
        'NLS_TIME_FORMAT' => 'HH24:MI:SS',
        'NLS_DATE_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
        'NLS_TIMESTAMP_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
        'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
        'NLS_NUMERIC_CHARACTERS' => '.,',
      ];

      if (isset($config['schema'])) {
        $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
      }

      $db->setSessionVars($sessionVars);
      return $db;
    });*/
    $capsule->setAsGlobal();
    $capsule->bootEloquent();


    foreach ((array) $container['database'] as $connectionName => $connectionConfig) {
      if ($connectionConfig->driver == 'mysql') {
        //$capsule->getDatabaseManager()->connection($connectionName)->setName($connectionName);
        $capsule->getDatabaseManager()->connection($connectionName);
      }
    }

  }
}
