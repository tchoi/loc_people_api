<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 09/04/18
 * Time: 09:54
 */

namespace LocPeopleApi\App\Controller;

use LocPeopleApi\App\Exception\HttpException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
class LoginController extends BaseController{
  public function login(Request $request, Response $response, $args){
    $ret = [
      'statusCode'=>401,
      'success'=>false,
      'msg'=>"Erro ao tentar realizar o login"
    ];
    $data = null;
    $data = $request->getParsedBody();
    $user = $this->authLogin($data);
    if(!$user['success']){
      $ret['msg'] = $ret['msg'] . "\n".implode("\n",$user['errors']);
    }else{
      $ret['statusCode'] = 200;
      $ret['success'] = true;
      $ret['msg'] = "Usuário logado com sucesso";
      $ret['user'] = $user['user'];
    }
    return json_encode($ret);
  }

  public function logoff(Request $request, Response $response, $args){
    $ret = [
        'statusCode'=>401,
        'success'=>false,
        'msg'=>"Erro ao tentar realizar o login"
    ];
    $data = null;
    $data = $request->getParsedBody();

    $this->repository("usuario")->logoff($data['id']);

    $ret['statusCode'] = 200;
    $ret['success'] = true;
    $ret['msg'] = "Usuário deslogado com sucesso";

    return json_encode($ret);
  }
}