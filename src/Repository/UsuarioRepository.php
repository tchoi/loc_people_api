<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 12:00
 */

namespace LocPeopleApi\Domain\Repository;


use LocPeopleApi\Domain\Model\Usuario;

class UsuarioRepository
{
  /*public function getUserByToken($token){
    $user = Usuario::where("token",$token)->first();
    if(!isset($user->id)){
      return false;
    }
    return $user;
  }*/

  public function getUserByEmail($email){
    $user = Usuario::where('email',$email)->first();
    if(isset($user->id)){
      return $user;
    }
    return false;
  }

  public function getUserByEmailSenha($email,$senha){
    $user = Usuario::where('email',$email)->where('senha',$senha)->first();
    if(isset($user->id)){
      return $user;
    }
    return false;
  }

  public function getUserByToken($token){
    $user = Usuario::whereRaw("md5(id) = '{$token}'")->first();
    if(isset($user->id)){
      return $user;
    }
    return false;
  }

  public function createUsuario($data){
    $user = new Usuario();
    $user->fill($data);
    if(isset($data['dispositivo_id'])){
      $user->dispositivo_id = $data['dispositivo_id'];
    }
    if(isset($data['dispositivo_modelo'])){
      $user->dispositivo_modelo = $data['dispositivo_modelo'];
    }
    if(isset($data['dispositivo_versao'])){
      $user->dispositivo_versao = $data['dispositivo_versao'];
    }
    if($user->save()){
      return $user;
    }
    return false;

  }

  public function saveUsuario($data){
    $user = Usuario::find($data['id']);
    if(isset($data['dispositivo_id'])){
      $user->dispositivo_id = $data['dispositivo_id'];
    }
    if(isset($data['dispositivo_modelo'])){
      $user->dispositivo_modelo = $data['dispositivo_modelo'];
    }
    if(isset($data['dispositivo_versao'])){
      $user->dispositivo_versao = $data['dispositivo_versao'];
    }
    $user->updated_at = date("Y-m-d H:i:s");
    if($user->save()){
      return $user;
    }
    return false;
  }

  public function logoff($token){
    $user = Usuario::whereRaw("md5(id) = '{$token}'")->first();
    if(isset($user->id)){
      $user->dispositivo_id = "";
      $user->dispositivo_modelo = "";
      $user->dispositivo_versao = "";
      $user->save();
      return $user;
    }
    return false;
  }
}