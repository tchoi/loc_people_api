<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:17
 */

namespace LocPeopleApi\App\Exception;


use \Exception;

class HttpException extends Exception
{
  protected $statusCode;
  protected $message;

  public function __construct($httpStatusCode = 500, array $data)
  {
    $this->statusCode = $httpStatusCode;
    $this->message = json_encode($data);
  }

  public function getStatusCode()
  {
    return $this->statusCode;
  }
}
