<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:37
 */

namespace LocPeopleApi\App\Controller;

use LocPeopleApi\App\Exception\HttpException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
class HomeController extends BaseController
{
  public function index(Request $request, Response $response, $args)
  {
    /**
     * Formas de retornas uma valor valido.
     */
    return $response->withStatus(201)->write(json_encode([
      'opa' => 1
    ]));

    /**
     * Sempre retorna 200 OK
     */
    return json_encode([
      'mensagem' => 'OK'
    ]);

    /**
     * Não converte para json
     */
    return 'Uma string qualquer';

    /**
     * Melhor usado para retornar erros
     */
    throw new HttpException(302, ['mensagem' => 'Falha de autenticação']);
  }
}
