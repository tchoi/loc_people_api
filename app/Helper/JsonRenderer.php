<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:16
 */

namespace LocPeopleApi\App\Helper;

use \Psr\Http\Message\ResponseInterface;

class JsonRenderer
{
  /**
   *
   * @param ResponseInterface $response
   * @param int $statusCode
   * @param array $data
   *
   * @return ResponseInterface
   *
   * @throws \InvalidArgumentException
   * @throws \RuntimeException
   */
  public function render(ResponseInterface $response, $data, $statusCode = 200)
  {
    $jsonResponse = $response->withHeader('Content-Type', 'application/json')
      ->withStatus($statusCode);

    $jsonResponse->getBody()->write(json_encode($data));
    return $jsonResponse;
  }
}
