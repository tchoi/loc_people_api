<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:07
 */
$container['repository.usuario'] = function () {
  return new \LocPeopleApi\Domain\Repository\UsuarioRepository();
};

$container['repository.localizacao'] = function () {
  return new \LocPeopleApi\Domain\Repository\LocalizacaoRepository();
};