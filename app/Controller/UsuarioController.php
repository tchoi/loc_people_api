<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 12/05/2018
 * Time: 16:34
 */

namespace LocPeopleApi\App\Controller;

use LocPeopleApi\App\Exception\HttpException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
class UsuarioController extends BaseController{
  protected function validateInsert($data){
    $v = new \Valitron\Validator($data);
    $v->rule('required', array('dispositivo_id', 'email','senha'))->message('O campo {field} é obrigatório');
    $v->labels(array(
        'dispositivo_id' => 'identificador do dispositivo',
        'email' => 'endereço de email',
        'senha' => 'senha'
    ));
    if(!$v->validate()){
      $aErr = array_column($v->errors(),0);
      return $aErr;
    }
    return true;
  }
  public function create(Request $request, Response $response, $args){
    $ret = [
        'statusCode'=>401,
        'success'=>false,
        'msg'=>"Erro ao tentar criar o usuário"
    ];
    $data = null;
    $data = $request->getParsedBody();
    $validate = $this->validateInsert($data);
    if($validate!==true){
      $ret['msg'] = $ret['msg'] ."\n". implode("\n",$validate);
      return json_encode($ret);
    }

    $save = $this->repository("usuario")->createUsuario($data);
    if($save!==false){
      $ret['statusCode'] = 201;
      $ret['success'] = true;
      $ret['msg'] = "Usuário criado com sucesso";
      $ret['user'] = $save;
    }
    return json_encode($ret);
  }

  public function update(Request $request, Response $response, $args){
    $ret = [
        'statusCode'=>401,
        'success'=>false,
        'msg'=>"Erro ao tentar salvar o usuário"
    ];
    $data = null;
    $data = $request->getParsedBody();
    $validate = $this->validateInsert($data);
    if($validate!==true){
      $ret['msg'] = $ret['msg'] ."\n". implode("\n",$validate);
      return json_encode($ret);
    }

    $save = $this->repository("usuario")->saveUsuario($data);
    if($save!==false){
      $ret['statusCode'] = 201;
      $ret['success'] = true;
      $ret['msg'] = "Usuário salvo com sucesso";
      $ret['user'] = $save;
    }
    return json_encode($ret);
  }
}