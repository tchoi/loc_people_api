<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:44
 */

namespace LocPeopleApi\Domain\Model;

class Usuario extends AbstractModel{
  protected $connection = 'mysql_main';
  protected $table = 'usuario';
  protected $primaryKey = 'id';
  public $timestamps = true;
  protected $fillable = ['nome','email','senha','dispositivo_id','dispositivo_modelo','dispositivo_versao'];
}