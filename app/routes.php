<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:05
 */
$app->group('', function () use ($app) {
  $app->get('/', '\LocPeopleApi\App\Controller\HomeController:index');

  $app->group('/api', function () use ($app) {
    $app->group('/v1', function () use ($app) {
      //Login
      $app->post("/login","\LocPeopleApi\App\Controller\LoginController:login");

      //Logoff
      $app->post("/logoff","\LocPeopleApi\App\Controller\LoginController:logoff");

      //Usuario
      $app->group("/usuario", function () use ($app) {
        $app->get('/', '\LocPeopleApi\App\Controller\HomeController:index');
        $app->post("/","\LocPeopleApi\App\Controller\UsuarioController:create");
        $app->put("/{id:[0-9]+}","\LocPeopleApi\App\Controller\UsuarioController:update");
      });

      //Localizacoes
      $app->group("/localizacao", function () use ($app) {
        $app->post("/filter","\LocPeopleApi\App\Controller\LocalizacaoController:filter");
        $app->get('/', '\LocPeopleApi\App\Controller\HomeController:index');
        $app->post("/","\LocPeopleApi\App\Controller\LocalizacaoController:create");
      });

    });
  });
})
//->add(new \QInstall\App\Middleware\JsonCache()) // Middleware experimental
->add(new \LocPeopleApi\App\Middleware\JsonResponse());