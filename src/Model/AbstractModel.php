<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:45
 */

namespace LocPeopleApi\Domain\Model;


use \Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{
  public $timestamps = false;

  public static function persist(array $data)
  {
    if (empty($data)) {
      throw new \Exception("Array vazia não pode ser persistida.");
    }

    $model = new static;
    $pk = $model->getKeyName();
    $buffer = $data;

    if (isset($data[$pk])) {
      $model = static::find($data[$pk]);
      $buffer = $model->toArray();

      foreach ($data as $field => $value) {
        if (isset($buffer[$field])) {
          $buffer[$field] = $data[$field];
        }
      }
    }

    if ($model->forceFill($buffer)->save()) {
      return $model;
    }

    return false;
  }
}
