<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:20
 */

namespace LocPeopleApi\App\Controller;

use LocPeopleApi\Domain\Repository\UsuarioRepository;
use \Interop\Container\ContainerInterface;

abstract class BaseController
{
  protected $container;
  protected $elapsed;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
    $this->elapsed = microtime(true);
  }

  public function repository($repository)
  {
    return $this->container["repository.{$repository}"];
  }

  public function authLogin($data){
    $v = new \Valitron\Validator($data);
    $v->rule('required', array('dispositivo_id', 'email','senha'))->message('O campo {field} é obrigatório');
    $v->labels(array(
      'dispositivo_id' => 'identificador do dispositivo',
      'email' => 'endereço de email',
      'senha' => 'senha'
    ));
    if(!$v->validate()){
      $aErr = array_column($v->errors(),0);
      return array("success"=>false,"errors"=>$aErr);
    }
    $user = $this->repository("usuario")->getUserByEmailSenha($data['email'],$data['senha']);
    if($user!==false){
      $data['id'] = $user->id;
      $updUser = $this->repository("usuario")->saveUsuario($data);
      $arr = array("success"=>false,"errors"=>array("Erro ao tentar atualizar informações do usuário"));
      if( isset($updUser->id)){
        $arr['success'] = true;
        unset($arr['errors']);
        $arr['user'] = $updUser;
      }
      return $arr;
    }
    return array("success"=>false,"errors"=>array("Não foi possivel encontrar o usuário"));
  }

  public function authLogon($data){

    $v = new \Valitron\Validator($data);
    $v->rule('required', array('token'))->message('O campo {field} é obrigatório');
    $v->labels(array(
      'token' => 'Token',
    ));
    if(!$v->validate()){
      $aErr = array_column($v->errors(),0);
      return array("success"=>false,"errors"=>$aErr);
    }
    $user = $this->repository("usuario")->getUserByToken($data['token']);
    if($user!==false){
      $data['id'] = $user->id;
      $updUser = $this->repository("usuario")->saveUsuario($data);
      $arr = array("success"=>false,"errors"=>array("Erro ao tentar atualizar informações do usuário"));
      if( isset($updUser->id)){
        $arr['success'] = true;
        unset($arr['errors']);
        $arr['user'] = $updUser;
      }
      return $arr;
    }
    return array("success"=>false,"errors"=>array("Não foi possivel encontrar o usuário"));
  }
}