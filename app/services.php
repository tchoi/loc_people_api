<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 27/03/18
 * Time: 11:08
 */
$container['json.renderer'] = function () {
  return new LocPeopleApi\App\Helper\JsonRenderer();
};